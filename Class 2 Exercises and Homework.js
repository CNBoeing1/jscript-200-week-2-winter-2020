// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
const me = {
    firstName : 'Christina',
    lastName : 'Nock',
    ['favorite food'] : 'cupcakes'
};
const bestFriend = {
	firstName : 'J',
    lastName : 'S',
   ['favorite food'] : 'steak'
};

var firstvar = bestFriend.firstName;
var secvar = me['favorite food'];
  console.log(`Best friends first name is ${bestFriend.firstName} and my favorite food is ${me['favorite food']}`); 

var htlmmyself = `
<h3>Objects</h3>
<div>
Best friends first name is ${firstvar} and my favorite food is ${secvar}
</div><br/><br/>`;
document.getElementById('myself').innerHTML = htlmmyself;

var items = [
  ['-', 'O','-'],
  ['-', 'X', 'O'],
  ['X', '-', 'X']
];

var writearray = function(arr){
  for(var i = 0; i < arr.length; i++){
    document.write(arr[i] + "\n");
  }
}
var writearray2 = function(arr, i){
	return arr[i];
  }


//Question 4


//console.log("Array after is: ");
//writearray(items);

var htlmtic = `
<h3>Array for tic tao toe Before change</h3>

<table>
	<tr>
		<th>${writearray2(items, 0)}</th>
	</tr>
	<tr>
		<th>${writearray2(items, 1)}</th>
	</tr>
	<tr>
		<th>${writearray2(items, 2)}</th>
	</tr>
	
</table>
<br/>`;
document.getElementById('tic').innerHTML = htlmtic;

items[0][2] = 'O';

var htlmtic2 = `
<h3>Array for tic tao toe After change</h3>
<table>
	<tr>
		<th>${writearray2(items, 0)}</th>
	</tr>
	<tr>
		<th>${writearray2(items, 1)}</th>
	</tr>
	<tr>
		<th>${writearray2(items, 2)}</th>
	</tr>
	
</table>
<br/><br/>`;
document.getElementById('tic2').innerHTML = htlmtic2;

var regex1 = RegExp('\\S+@\\S+\.\\w{3}').test('boo@baz.com');
var regex2 = RegExp('\\S+@\\S+\.\\w{3}').test('brett@ mall');

var htlmregex = `
<h3>Regex tests</h3>
<div>
For boo email: ${regex1} 
<br/>
For brett mail: ${regex2}
</div><br/><br/>`;
document.getElementById('regex').innerHTML = htlmregex;
//brett@ mall
//console.log("Regex test is " + RegExp('\\S+@\\S+\.\\w{3}').test('boo@baz.com'));

var string = "1/21/2019";
var datetostring = string.split("/");

var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var getdeadline = function(day){
  
var dueDate = new Date(day);
  dueDate.setDate(day.getDate() + 7);
  return dueDate;
}

var dateobj = new Date(datetostring[2], (datetostring[0].padStart(2) - 1), datetostring[1].padStart(2));
//console.log("date object is " + dateobj);
var htlmdate1 = `
<h3>String to Date object</h3>
	${dateobj}
<br/>`;
document.getElementById('moredate').innerHTML = htlmdate1;


var dudate = getdeadline(dateobj);

var htlmdate2 = `
<h3>Due date</h3>
	${dudate}
<br/>`;
document.getElementById('moredate2').innerHTML = htlmdate2;

var monthpad = (dudate.getMonth() + 1)+"";
var monthpadded = monthpad.padStart(2,"0");

var htlmdate3 = `
<h3>HTML string</h3>
<time datetime="${dudate.getFullYear()}-${monthpadded}-${dudate.getDate()}">${months[dudate.getMonth()]} ${dudate.getDate()}, ${dudate.getFullYear()}</time>

<br/>`;
document.getElementById('moredate3').innerHTML = htlmdate3;
//console.log("due date is " + getdeadline(dudate));

//'<time datetime="2018-01-14">January 14, 2018</time>'
//console.log(`<time datetime="${dudate.getFullYear()}-${monthpadded}-${dudate.getDate()}">${months[dudate.getMonth()]} ${dudate.getDate()}, ${dudate.getFullYear()}</time> `);


// 2. console.log best friend's firstName and your favorite food


// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X


// 4. After the array is created, 'O' claims the top right square.
// Update that value.


// 5. Log the grid to the console.


// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test


// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
//const assignmentDate = '1/21/2019';


// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.


// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
//const months = [
 // 'January',
  //'February',
 // 'March',
 // 'April',
 // 'May',
 // 'June',
 // 'July',
//  'August',
//  'September',
//  'October',
//  'November',
//  'December'
//];


// 10. log this value using console.log
